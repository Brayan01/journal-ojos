def get_stages(slices):

    new_data = np.zeros((2080,210,300,4), dtype='uint8') #Arreglo para guardar datos
    index = 0
    rgb_index = []
    y = []
    count = 0

    for i in range(26): #Number of patients 13-parkinson, 13-control
        for j in range(5): #Number of samples
            for k in range(4): #From each sample is extracted 4 chanels of color (R,G,B,Gray)
                for l in range(4):  #Data augmentation Original_slice, horizontal flip, vertical flip and horizontal o vertical flip

                    new_data[index,:,:,:] = slices[j+i*5,k,l]

                    if count <1040:        #todos los pacientes control
                        y.append(0)

                    if 1040<=count<1200:   #del 14 al 15
                        y.append(3)

                    if 1200<=count<1280 or 1520<=count<1680 or 1840<=count<2080:   #del 16,20-21,24-26
                        y.append(2)
                        
                    if 1280<=count<1520 or 1680<=count<1840:   # del 17-19,22-23
                        y.append(1)

                    count += 1

    return new_data, np.array(y)